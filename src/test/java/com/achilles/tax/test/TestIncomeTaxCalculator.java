package com.achilles.tax.test;

import com.achilles.tax.IncomeTaxCalculator;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class TestIncomeTaxCalculator {

    @Test
    public void testUnder50MIncome() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();

        BigDecimal result = incomeTaxCalculator.compute(new BigDecimal(50000000));

        Assert.assertEquals(new BigDecimal(2500000), result);
    }

    @Test
    public void testUnder250MIncome() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();

        BigDecimal result = incomeTaxCalculator.compute(new BigDecimal(250000000));

        Assert.assertEquals(new BigDecimal(32500000), result);
    }

    @Test
    public void testUnder500MIncome() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();

        BigDecimal result = incomeTaxCalculator.compute(new BigDecimal(500000000));

        Assert.assertEquals(new BigDecimal(95000000), result);
    }

    @Test
    public void testUnder600MIncome() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();

        BigDecimal result = incomeTaxCalculator.compute(new BigDecimal(600000000));

        Assert.assertEquals(new BigDecimal(125000000), result);
    }

    @Test
    public void testTaxReliefSingle() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();
        BigDecimal taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(54000000), IncomeTaxCalculator.RELIEF_TYPE_SINGLE);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(50000000), IncomeTaxCalculator.RELIEF_TYPE_SINGLE);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(55000000), IncomeTaxCalculator.RELIEF_TYPE_SINGLE);

        Assert.assertEquals(new BigDecimal(1000000), taxableIncome);
    }

    @Test
    public void testTaxReliefMarriedNoDependant() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();
        BigDecimal taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(58500000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(50000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(60000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED);

        Assert.assertEquals(new BigDecimal(1500000), taxableIncome);
    }

    @Test
    public void testTaxReliefMarried1Dependant() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();
        BigDecimal taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(63000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_1_DEPENDANT);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(50000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_1_DEPENDANT);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(64000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_1_DEPENDANT);

        Assert.assertEquals(new BigDecimal(1000000), taxableIncome);
    }

    @Test
    public void testTaxReliefMarried2Dependant() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();
        BigDecimal taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(67500000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_2_DEPENDANT);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(50000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_2_DEPENDANT);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(68500000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_2_DEPENDANT);

        Assert.assertEquals(new BigDecimal(1000000), taxableIncome);
    }

    @Test
    public void testTaxReliefMarried3Dependant() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();
        BigDecimal taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(72000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_3_DEPENDANT);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(50000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_3_DEPENDANT);

        Assert.assertEquals(new BigDecimal(0), taxableIncome);

        taxableIncome = incomeTaxCalculator.computeTaxableIncomeAfterRelief(new BigDecimal(73000000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_3_DEPENDANT);

        Assert.assertEquals(new BigDecimal(1000000), taxableIncome);
    }

    @Test
    public void testChainedFlow() {
        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();
        BigDecimal result = incomeTaxCalculator.compute(new BigDecimal(6500000), IncomeTaxCalculator.RELIEF_TYPE_MARRIED_1_DEPENDANT);

        Assert.assertEquals(new BigDecimal(750000), result);

        result = incomeTaxCalculator.compute(new BigDecimal(25000000), IncomeTaxCalculator.RELIEF_TYPE_SINGLE);
        Assert.assertEquals(new BigDecimal(31900000), result);
    }
}
