import com.achilles.tax.IncomeTaxCalculator;

import java.math.BigDecimal;
import java.util.Scanner;

class MainApp {

    public static void main(String[] args) {

        String input = "";
        BigDecimal monthlySalary = null;
        String selectedTaxRelief;
        Scanner in = new Scanner(System.in);

        System.out.println("Indonesia Personal Income Tax Calculator");
        System.out.println("Press q to quit.");

        IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();

        do {
            printSalaryInstructions();
            input = in.nextLine();
            monthlySalary = getMonthlySalary(input);

            if (monthlySalary == null) {
                System.out.println("Invalid monthly salary. Please try again.");
            }
        } while (monthlySalary == null);


        do {
            printProfileInstructions();

            input = in.nextLine();
            selectedTaxRelief = getSelectedTaxRelief(input);

            if (selectedTaxRelief == null) {
                System.out.println("Invalid option. Please try again.");
            }
        } while (selectedTaxRelief == null);


        BigDecimal result = incomeTaxCalculator.compute(monthlySalary, selectedTaxRelief);
        System.out.println("Payable Tax: " + result + " IDR");
    }

    private static void printSalaryInstructions() {
        System.out.println("Enter monthly salary: ");
    }

    private static BigDecimal getMonthlySalary(String input) {
        try {
            return new BigDecimal(input);
        } catch (Exception ignored) {
        }
        return null;
    }

    private static void printProfileInstructions() {
        System.out.println("Select one of the profile below ");
        System.out.println("(1) Single ");
        System.out.println("(2) Married ");
        System.out.println("(3) Married with 1 dependent ");
        System.out.println("(4) Married with 2 dependent ");
        System.out.println("(5) Married with 3 dependent ");
    }

    private static String getSelectedTaxRelief(String input) {

        try {
            int i = Integer.parseInt(input);

            switch (i) {
                case 1:
                    return IncomeTaxCalculator.RELIEF_TYPE_SINGLE;
                case 2:
                    return IncomeTaxCalculator.RELIEF_TYPE_MARRIED;
                case 3:
                    return IncomeTaxCalculator.RELIEF_TYPE_MARRIED_1_DEPENDANT;
                case 4:
                    return IncomeTaxCalculator.RELIEF_TYPE_MARRIED_2_DEPENDANT;
                case 5:
                    return IncomeTaxCalculator.RELIEF_TYPE_MARRIED_3_DEPENDANT;
                default:
                    return null;
            }
        } catch (NumberFormatException ignored) {
        }
        return null;
    }
}
