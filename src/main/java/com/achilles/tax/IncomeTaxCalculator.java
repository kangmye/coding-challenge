package com.achilles.tax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IncomeTaxCalculator {

    private static final BigDecimal TOTAL_MONTHS = new BigDecimal(12);

    private static final BigDecimal TAX_RATE_LVL_1 = new BigDecimal(5);
    private static final BigDecimal TAX_RATE_LVL_2 = new BigDecimal(15);
    private static final BigDecimal TAX_RATE_LVL_3 = new BigDecimal(25);
    private static final BigDecimal TAX_RATE_LVL_4 = new BigDecimal(30);

    public static final String RELIEF_TYPE_SINGLE = "TK0";
    public static final String RELIEF_TYPE_MARRIED = "K0";
    public static final String RELIEF_TYPE_MARRIED_1_DEPENDANT = "K1";
    public static final String RELIEF_TYPE_MARRIED_2_DEPENDANT = "K2";
    public static final String RELIEF_TYPE_MARRIED_3_DEPENDANT = "K3";

    private List<TaxRate> taxRates;
    private Map<String, BigDecimal> taxReliefs;

    public IncomeTaxCalculator() {
        initTaxRates();
        initTaxReliefs();
    }

    private void initTaxReliefs() {
        taxReliefs = new HashMap<>();

        taxReliefs.put(RELIEF_TYPE_SINGLE, new BigDecimal(54000000));
        taxReliefs.put(RELIEF_TYPE_MARRIED, new BigDecimal(58500000));
        taxReliefs.put(RELIEF_TYPE_MARRIED_1_DEPENDANT, new BigDecimal(63000000));
        taxReliefs.put(RELIEF_TYPE_MARRIED_2_DEPENDANT, new BigDecimal(67500000));
        taxReliefs.put(RELIEF_TYPE_MARRIED_3_DEPENDANT, new BigDecimal(72000000));
    }

    private void initTaxRates() {
        taxRates = new ArrayList<>();

        taxRates.add(new TaxRate(new BigDecimal(0), new BigDecimal(50000000), TAX_RATE_LVL_1));
        taxRates.add(new TaxRate(new BigDecimal(50000000), new BigDecimal(250000000), TAX_RATE_LVL_2));
        taxRates.add(new TaxRate(new BigDecimal(250000000), new BigDecimal(500000000), TAX_RATE_LVL_3));
        taxRates.add(new TaxRate(new BigDecimal(500000000), null, TAX_RATE_LVL_4));
    }

    public BigDecimal compute(BigDecimal monthlySalary, String reliefType) {
        BigDecimal annualIncome = computeAnnualIncome(monthlySalary);
        BigDecimal taxableIncome = computeTaxableIncomeAfterRelief(annualIncome, reliefType);
        return compute(taxableIncome);
    }

    /**
     * Compute annual income based on monthly salary
     *
     * @param monthlySalary
     * @return
     */
    public BigDecimal computeAnnualIncome(BigDecimal monthlySalary) {
        return monthlySalary.multiply(TOTAL_MONTHS);
    }

    /**
     * Compute taxable income based on Profile
     *
     * @param annualIncome
     * @param taxReliefType
     * @return
     */
    public BigDecimal computeTaxableIncomeAfterRelief(BigDecimal annualIncome, String taxReliefType) {
        BigDecimal taxReliefAmount = taxReliefs.get(taxReliefType);
        BigDecimal result = new BigDecimal(0);
        if (annualIncome.compareTo(taxReliefAmount) > 0) {
            result = annualIncome.subtract(taxReliefAmount);
        }
        return result;
    }

    /**
     * Compute tax amount based on the taxable annual income
     *
     * @param annualIncome taxable annual income
     * @return
     */
    public BigDecimal compute(BigDecimal annualIncome) {

        boolean aboveLowerLimit;
        boolean aboveUpperLimit;

        BigDecimal result = new BigDecimal(0);
        BigDecimal amountSubjectedToTax;

        for (TaxRate taxRate : taxRates) {
            aboveLowerLimit = annualIncome.compareTo(taxRate.getLowerLimit()) > 0;
            aboveUpperLimit = taxRate.getUpperLimit() != null && annualIncome.compareTo(taxRate.getUpperLimit()) > 0;

            if (aboveLowerLimit) {
                // if exceed upper limit, the amount = upper - lower, otherwise = annual - lower
                amountSubjectedToTax = aboveUpperLimit
                        ? taxRate.getUpperLimit().subtract(taxRate.getLowerLimit())
                        : annualIncome.subtract(taxRate.getLowerLimit());

                result = result.add(taxRate.getRate().multiply(amountSubjectedToTax));
            }
        }

        return result.setScale(0);
    }
}
