package com.achilles.tax;

import java.math.BigDecimal;
import java.math.RoundingMode;

class TaxRate {
    private BigDecimal lowerLimit;
    private BigDecimal upperLimit;
    private BigDecimal rate;

    public TaxRate(BigDecimal lowerLimit, BigDecimal upperLimit, BigDecimal rate) {
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        this.rate = rate.divide(new BigDecimal(100));
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public BigDecimal getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(BigDecimal upperLimit) {
        this.upperLimit = upperLimit;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}